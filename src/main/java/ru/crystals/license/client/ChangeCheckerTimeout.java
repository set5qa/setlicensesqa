package ru.crystals.license.client;

import java.lang.reflect.*;

/**
 * Created by d.fedorov on 21.12.2018.
 */
public class ChangeCheckerTimeout {

        public static void changeTimeout() throws Exception {
            final long timeout =1000;
            Field field = LicenseChecker.class.getDeclaredField("THREAD_TIMEOUT");
            field.setAccessible(true);

            Field modifiersField = Field.class.getDeclaredField("modifiers");
            modifiersField.setAccessible(true);
            modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);

            field.setLong(null,timeout);
        }
}


package SetLicenseTest.licenseTest;

import SetLicenseTest.utils.agentWork.AgentStats;
import SetLicenseTest.utils.agentWork.AgentWork;
import SetLicenseTest.utils.clientWork.Client;
import SetLicenseTest.utils.clientWork.ClientWork;
import SetLicenseTest.utils.clientWork.SystemTimeShifter;
import SetLicenseTest.utils.databaseworking.DataBaseWork;
import io.qameta.allure.Description;
import io.qameta.allure.Issue;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.crystals.license.client.ChangeCheckerTimeout;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by d.fedorov on 18.01.2018.
 */
public class IntegrationTest {

    private DataBaseWork dataBaseWork = new DataBaseWork();
    private String workCatalog = System.getProperty("user.dir") + "/src/main/java/SetLicenseTest/licenseTest/";
    private String stat = System.getProperty("user.dir") + "/src/main/java/fillDBExample/stat.json";
    private String client = System.getProperty("user.dir") + "/src/main/java/fillDBExample/clientcatalog2.json";
    private String licenseCount = System.getProperty("user.dir") + "/src/main/java/fillDBExample/licensecount.json";

    private String productWithModules;
    private String agentCatalog;
    private String clientCatalog;

    @BeforeClass(description = "Загрузка данных в БД перед всеми тестами")
    public void initLogger() throws SQLException, Exception {
        String productCatalog = workCatalog + "productcatalog.json";
        String moduleCatalog = workCatalog + "modulecatalog.json";

        File fileProductCatalog = new File(productCatalog);
        File fileModuleCatalog = new File(moduleCatalog);

        ClientWork.initLogger();
        ChangeCheckerTimeout.changeTimeout();

        dataBaseWork.clearData();
        dataBaseWork.clearCatalog();

        if (fileProductCatalog.exists()) {
            dataBaseWork.loadProductCatalog(productCatalog);
        }

        if (fileModuleCatalog.exists()) {
            dataBaseWork.loadModuleCatalog(moduleCatalog);
        }
    }

    @BeforeMethod(description = "Загрузка данных в БД перед каждым тестом")
    public void setConfigs(Method method) throws SQLException, IOException, InterruptedException {

        AgentWork agent = new AgentWork();
        agent.stopAgent();
        Thread.sleep(1000);

        productWithModules = workCatalog + method.getName() + "/productWithModules.json";
        agentCatalog = workCatalog + method.getName() + "/config.conf";
        clientCatalog = workCatalog + method.getName() + "/clientcatalog.json";
        licenseCount = workCatalog + method.getName() + "/licensecount.json";
        client = workCatalog + method.getName();
        String licensecatalog = workCatalog.replace("/src/main/java/SetLicenseTest/licenseTest/", "") + "/licenses";

        File fileProductWithModules = new File(productWithModules);
        File fileLicenseCatalog = new File(licensecatalog);

        ClientWork.deleteFolder(fileLicenseCatalog);
        dataBaseWork.clearData();

        if (fileProductWithModules.exists()) {
            dataBaseWork.insertProductsWithModules(productWithModules);
        }
    }

    @Test
    @Description("Включение режима оффлайн при неудачной попытки коннекта к агенту")
    @Severity(SeverityLevel.BLOCKER)
    @Issue("SL-7")
    public void test_UpdateOfflineModeOn() throws Exception {

        AgentWork agent = new AgentWork(agentCatalog);

        agent.configureAndStart(true);

        dataBaseWork.waitToken(agent.getSetId(), 15000, 1000);

        agent.setAndCheckHid(dataBaseWork.getHid(agent.getSetId()), false, false);

        Client myClient = new Client(clientCatalog, true);
        ClientWork setretail10 = new ClientWork(myClient, agent.getHost(), agent.getPort());

        Assert.assertTrue(setretail10.waitIfAgentOnline());
        setretail10.checkModules(myClient, true);
        System.out.println("Current time " + new Date());
        agent.stopAgent();
        SystemTimeShifter.setOffset(7200000);  // 2 hours
        System.out.println("Changed Current time " + new Date());

        setretail10.sendClientInfo();
        Assert.assertFalse(setretail10.waitIfAgentOnline());
        setretail10.checkModules(myClient, true);

        setretail10.stopCheckerResetStatus();
        SystemTimeShifter.reset();
        System.out.println("Current time " + new Date());

    }

    @Test
    @Description("Включение режима оффлайн при неудачной попытки коннекта к агенту, прекращение работы после 10 дней в режиме оффлайн, восстановление работы  после включения агента")
    @Severity(SeverityLevel.BLOCKER)
    @Issue("SL-7")
    public void test_UpdateOfflinePeriodExpired() throws Exception {

        AgentWork agent = new AgentWork(agentCatalog);

        agent.configureAndStart(true);

        dataBaseWork.waitToken(agent.getSetId(), 15000, 1000);

        agent.setAndCheckHid(dataBaseWork.getHid(agent.getSetId()), false, false);

        Client myClient = new Client(clientCatalog, true);
        ClientWork setretail10 = new ClientWork(myClient, agent.getHost(), agent.getPort());

        Assert.assertTrue(setretail10.waitIfAgentOnline());
        setretail10.checkModules(myClient, true);
        System.out.println("Current time " + new Date());
        agent.stopAgent();
        SystemTimeShifter.setOffset(2 * 3600 * 1000);  // 2 hours
        System.out.println("Changed Current time " + new Date());

        setretail10.sendClientInfo();
        Assert.assertFalse(setretail10.waitIfAgentOnline());
        setretail10.checkModules(myClient, true);
        // set time 5 day in future
        SystemTimeShifter.changeOffset(5 * 24 * 3600 * 1000);
        setretail10.sendClientInfo();
        Assert.assertFalse(setretail10.waitIfAgentOnline());
        setretail10.checkModules(myClient, true);

        SystemTimeShifter.changeOffset(5 * 24 * 3600 * 1000 + 3600 * 1000);
        setretail10.sendClientInfo();
        Assert.assertTrue(setretail10.waitIfOfflinePeriodExpired());
        setretail10.checkModules(myClient, false);

        agent.startAgent();

        setretail10.sendClientInfo();
        Assert.assertTrue(setretail10.waitIfAgentOnline());
        setretail10.checkModules(myClient, true);

        setretail10.stopCheckerResetStatus();
        SystemTimeShifter.reset();
        System.out.println("Current time " + new Date());

    }

    @Test
    @Description("Включение режима оффлайн при неудачной попытки коннекта к агенту, восстановление работы, повторный вход  в режим оффлайн")
    @Severity(SeverityLevel.BLOCKER)
    @Issue("SL-7")
    public void test_UpdateOfflinePeriodRecalc() throws Exception {

        AgentWork agent = new AgentWork(agentCatalog);

        agent.configureAndStart(true);

        dataBaseWork.waitToken(agent.getSetId(), 15000, 1000);

        agent.setAndCheckHid(dataBaseWork.getHid(agent.getSetId()), false, false);

        Client myClient = new Client(clientCatalog, true);
        ClientWork setretail10 = new ClientWork(myClient, agent.getHost(), agent.getPort());

        Assert.assertTrue(setretail10.waitIfAgentOnline());
        setretail10.checkModules(myClient, true);
        System.out.println("Current time " + new Date());
        agent.stopAgent();
        SystemTimeShifter.setOffset(2 * 3600 * 1000);  // 2 hours
        System.out.println("Changed Current time " + new Date());

        setretail10.sendClientInfo();
        Assert.assertFalse(setretail10.waitIfAgentOnline());
        setretail10.checkModules(myClient, true);
        // set time 5 day in future
        SystemTimeShifter.changeOffset(5 * 24 * 3600 * 1000);
        setretail10.sendClientInfo();
        Assert.assertFalse(setretail10.waitIfAgentOnline());
        setretail10.checkModules(myClient, true);

        agent.startAgent();

        setretail10.sendClientInfo();
        Assert.assertTrue(setretail10.waitIfAgentOnline());
        setretail10.checkModules(myClient, true);

        System.out.println("Current time " + new Date());
        agent.stopAgent();
        SystemTimeShifter.changeOffset(2 * 3600 * 1000);  // 2 hours
        System.out.println("Changed Current time " + new Date());

        setretail10.sendClientInfo();
        Assert.assertFalse(setretail10.waitIfAgentOnline());
        setretail10.checkModules(myClient, true);
        // set time 6 day in future
        SystemTimeShifter.changeOffset(6 * 24 * 3600 * 1000);
        setretail10.sendClientInfo();
        Assert.assertFalse(setretail10.waitIfAgentOnline());
        setretail10.checkModules(myClient, true);

        setretail10.stopCheckerResetStatus();
        SystemTimeShifter.reset();
        System.out.println("Current time " + new Date());
    }

    @Test
    @Description("Тест-проверка работы со временм")
    @Severity(SeverityLevel.BLOCKER)
    @Issue("SL-7")
    public void test_timeExample() throws Exception {

        // System.out.println("True time" + new Timestamp(System.currentTimeMillis()).getTime() + "Change time" + System.currentTimeMillis());
        // Date date = new Date();
        // System.out.println("This Date " + date);
        //
        System.out.println("Current time " + new Date());

        SystemTimeShifter.setOffset(7200000);  // 2 hours
        // System.out.println("True time" + new Timestamp(System.currentTimeMillis()).getTime() + "Change time" + System.currentTimeMillis());
        // System.out.println("Changed Current time " + new Date());
        // System.out.println("This Date " + date);

        SystemTimeShifter.changeOffset(3600 * 24 * 5 * 1000 + 3600 * 1000);
        // System.out.println("True time" + new Timestamp(System.currentTimeMillis()).getTime() + "Change time" + System.currentTimeMillis());
        System.out.println("Changed Current time " + new Date());
        // System.out.println("This Date " + date);

        SystemTimeShifter.reset();
        // System.out.println("True time" + new Timestamp(System.currentTimeMillis()).getTime() + "Change time" + System.currentTimeMillis());

        System.out.println("Changed Current time " + new Date());
        // System.out.println("This Date " + date);

    }

    @Test
    @Description("Успешное получение лицензии при первом запуске агента и получение лицензии при первом запуске модуля")
    @Severity(SeverityLevel.BLOCKER)
    @Issue("SL-7")
    public void test_firstSuccessLicenseAgentModule() throws Exception {

        AgentWork agent = new AgentWork(agentCatalog);

        agent.configureAndStart(true);

        dataBaseWork.waitToken(agent.getSetId(), 15000, 1000);

        agent.setAndCheckHid(dataBaseWork.getHid(agent.getSetId()), false, false);

        Client myClient = new Client(clientCatalog, true);
        ClientWork setretail10 = new ClientWork(myClient, agent.getHost(), agent.getPort());

        Assert.assertTrue(setretail10.waitIfAgentOnline());
        setretail10.checkModules(myClient, true);

        agent.waitUntilConnectTimeChanged(agent.getAgentlastSuceededTime());

        // TODO: REMOVE AND ADD ASSERT
        myClient.saveGuidFromDb(dataBaseWork);
        String s = dataBaseWork.getAgentStat(agent.getSetId());
        System.out.println(s);
        AgentStats agentStats = new AgentStats(s);
        agentStats.checkClient(myClient);
        agentStats.checkResult(licenseCount);
        setretail10.stopCheckerResetStatus();
    }

    @Test
    @Description("Успешное получение лицензии на продукт и функции для нескольких модулей")
    @Severity(SeverityLevel.BLOCKER)
    @Issue("SL-7")
    public void test_firstSuccessLicenseSeveralModules() throws Exception {

        AgentWork agent = new AgentWork(agentCatalog);

        agent.configureAndStart(true);

        dataBaseWork.waitToken(agent.getSetId(), 25000, 1000);

        agent.setAndCheckHid(dataBaseWork.getHid(agent.getSetId()), false, false);

        Client client1 = new Client(client + "/client1.json", true);
        ClientWork setClient1 = new ClientWork(client1, agent.getHost(), agent.getPort());
        Assert.assertTrue(setClient1.waitIfAgentOnline());
        setClient1.checkModules(client1, true);

        Client client2 = new Client(client + "/client2.json", true);
        ClientWork setClient2 = new ClientWork(client2, agent.getHost(), agent.getPort());
        Assert.assertTrue(setClient2.waitIfAgentOnline());
        setClient2.checkModules(client2, true);

        Client client3 = new Client(client + "/client3.json", true);
        ClientWork setClient3 = new ClientWork(client3, agent.getHost(), agent.getPort());
        Assert.assertTrue(setClient3.waitIfAgentOnline());
        setClient3.checkModules(client3, true);

        agent.waitUntilConnectTimeChanged(agent.getAgentlastSuceededTime());

        // TODO: REMOVE AND ADD ASSERT
        client1.saveGuidFromDb(dataBaseWork);
        client2.saveGuidFromDb(dataBaseWork);
        client3.saveGuidFromDb(dataBaseWork);
        String s = dataBaseWork.getAgentStat(agent.getSetId());
        System.out.println(s);
        AgentStats agentStats = new AgentStats(s);
        agentStats.checkClient(client1);
        agentStats.checkClient(client2);
        agentStats.checkClient(client3);
        agentStats.checkResult(licenseCount);
        setClient1.stopCheckerResetStatus();
        setClient2.stopCheckerResetStatus();
        setClient3.stopCheckerResetStatus();
    }

    @Test
    @Description("Успешное получение лицензии на продукт и функции для нескольких модулей, не для всех модулей достаточно лицензий на функции")
    @Severity(SeverityLevel.BLOCKER)
    @Issue("SL-7")
    public void test_firstSuccessLicenseNotEnoughModules() throws Exception {

        AgentWork agent = new AgentWork(agentCatalog);

        agent.configureAndStart(true);

        dataBaseWork.waitToken(agent.getSetId(), 25000, 1000);

        agent.setAndCheckHid(dataBaseWork.getHid(agent.getSetId()), false, false);

        Client client1 = new Client(client + "/client1.json", true);
        ClientWork setClient1 = new ClientWork(client1, agent.getHost(), agent.getPort());
        Assert.assertTrue(setClient1.waitIfAgentOnline());
        setClient1.checkModules(client1, true);

        Client client2 = new Client(client + "/client2.json", true);
        ClientWork setClient2 = new ClientWork(client2, agent.getHost(), agent.getPort());
        Assert.assertTrue(setClient2.waitIfAgentOnline());
        setClient2.checkModules(client2, true);

        Client client3 = new Client(client + "/client3.json", true);
        ClientWork setClient3 = new ClientWork(client3, agent.getHost(), agent.getPort());
        Assert.assertTrue(setClient3.waitIfAgentOnline());
        System.out.println("Online");
        setClient3.checkModules(client3, true);

        agent.waitUntilConnectTimeChanged(agent.getAgentlastSuceededTime());

        // // TODO: REMOVE AND ADD ASSERT
        client1.saveGuidFromDb(dataBaseWork);
        client2.saveGuidFromDb(dataBaseWork);
        client3.saveGuidFromDb(dataBaseWork);
        String s = dataBaseWork.getAgentStat(agent.getSetId());
        System.out.println(s);
        AgentStats agentStats = new AgentStats(s);
        agentStats.checkClient(client1);
        agentStats.checkClient(client2);
        agentStats.checkClient(client3);
        agentStats.checkResult(licenseCount);
        setClient1.stopCheckerResetStatus();
        setClient2.stopCheckerResetStatus();
        setClient3.stopCheckerResetStatus();
    }

    @Test
    @Description("Успешное получение лицензии на продукт и функции для нескольких модулей, не для всех модулей" +
        " достаточно лицензий на функции и продукты")
    @Severity(SeverityLevel.BLOCKER)
    @Issue("SL-7")
    public void test_firstSuccessLicenseNotEnoughModulesAndProducts() throws Exception {

        AgentWork agent = new AgentWork(agentCatalog);

        agent.configureAndStart(true);

        dataBaseWork.waitToken(agent.getSetId(), 25000, 1000);

        agent.setAndCheckHid(dataBaseWork.getHid(agent.getSetId()), false, false);

        Client client1 = new Client(client + "/client1.json", true);
        ClientWork setClient1 = new ClientWork(client1, agent.getHost(), agent.getPort());
        Assert.assertTrue(setClient1.waitIfAgentOnline());
        setClient1.checkModules(client1, true);

        Client client2 = new Client(client + "/client2.json", true);
        ClientWork setClient2 = new ClientWork(client2, agent.getHost(), agent.getPort());
        Assert.assertTrue(setClient2.waitIfAgentOnline());
        setClient2.checkModules(client2, true);

        Client client3 = new Client(client + "/client3.json", true);
        ClientWork setClient3 = new ClientWork(client3, agent.getHost(), agent.getPort());
        Assert.assertTrue(setClient3.waitIfAgentOnline());
        setClient3.checkModules(client3, false);

        agent.waitUntilConnectTimeChanged(agent.getAgentlastSuceededTime());

        // // TODO:REMOVE ME
        // // Thread.sleep(10000);
        //
        // // TODO: REMOVE AND ADD ASSERT
        client1.saveGuidFromDb(dataBaseWork);
        client2.saveGuidFromDb(dataBaseWork);
        client3.saveGuidFromDb(dataBaseWork);
        String s = dataBaseWork.getAgentStat(agent.getSetId());
        System.out.println(s);
        AgentStats agentStats = new AgentStats(s);
        agentStats.checkClient(client1);
        agentStats.checkClient(client2);
        agentStats.checkClient(client3);
        agentStats.checkResult(licenseCount);
        setClient1.stopCheckerResetStatus();
        setClient2.stopCheckerResetStatus();
        setClient3.stopCheckerResetStatus();
    }

    @Test
    @Description("Успешное получение лицензии на продукт и функции для нескольких модулей, лицензий на модули больше чем на продукты")
    @Severity(SeverityLevel.BLOCKER)
    @Issue("SL-7")
    public void test_firstSuccessLicenseNotEnoughProducts() throws Exception {

        AgentWork agent = new AgentWork(agentCatalog);

        agent.configureAndStart(true);

        dataBaseWork.waitToken(agent.getSetId(), 25000, 1000);

        agent.setAndCheckHid(dataBaseWork.getHid(agent.getSetId()), false, false);

        Client client1 = new Client(client + "/client1.json", true);
        ClientWork setClient1 = new ClientWork(client1, agent.getHost(), agent.getPort());
        Assert.assertTrue(setClient1.waitIfAgentOnline());
        setClient1.checkModules(client1, true);

        Client client2 = new Client(client + "/client2.json", true);
        ClientWork setClient2 = new ClientWork(client2, agent.getHost(), agent.getPort());
        Assert.assertTrue(setClient2.waitIfAgentOnline());
        setClient2.checkModules(client2, true);

        Client client3 = new Client(client + "/client3.json", true);
        ClientWork setClient3 = new ClientWork(client3, agent.getHost(), agent.getPort());
        Assert.assertTrue(setClient3.waitIfAgentOnline());
        setClient3.checkModulesNoMainPermission(client3, false);

        agent.waitUntilConnectTimeChanged(agent.getAgentlastSuceededTime());

        // TODO: REMOVE AND ADD ASSERT
        client1.saveGuidFromDb(dataBaseWork);
        client2.saveGuidFromDb(dataBaseWork);
        client3.saveGuidFromDb(dataBaseWork);
        String s = dataBaseWork.getAgentStat(agent.getSetId());
        System.out.println(s);
        AgentStats agentStats = new AgentStats(s);
        agentStats.checkClient(client1);
        agentStats.checkClient(client2);
        agentStats.checkClient(client3);
        agentStats.checkResult(licenseCount);
        setClient1.stopCheckerResetStatus();
        setClient2.stopCheckerResetStatus();
        setClient3.stopCheckerResetStatus();
    }

    @Description("Получение лицензии при первом запуске,на сервере нет указанного SetID")
    @Severity(SeverityLevel.BLOCKER)
    @Issue("SL-7")
    @Test
    public void test_firstStartNoSetID() throws Exception {

        AgentWork agent = new AgentWork(agentCatalog);
        agent.configureAndStart(true);

        dataBaseWork.waitToken(agent.getSetId(), 15000, 1000);

        agent.setAndCheckHid(dataBaseWork.getHid(agent.getSetId()), true, false);

        Thread.sleep(10000);

        Client myClient = new Client(clientCatalog, false);
        ClientWork setretail10 = new ClientWork(myClient, agent.getHost(), agent.getPort());

        Thread.sleep(10000);

        Assert.assertFalse(setretail10.waitIfAgentOnline());
        setretail10.checkModules(myClient, false);
        setretail10.stopCheckerResetStatus();
    }

    // @Description("Получение лицензии, SetId привязан к другому клиенту")
    // @Severity(SeverityLevel.BLOCKER)
    // @Issue("SL-7")
    // @Test
    // public void test_setIDLinkedToOtherAgent() throws Exception {
    //
    // AgentWork agent = new AgentWork(agentCatalog);
    //
    // agent.configureAndStart(true);
    //
    // dataBaseWork.waitToken(agent.getSetId(), 15000, 1000);
    //
    // agent.setAndCheckHid(dataBaseWork.getHid(agent.getSetId()), false, false);
    //
    // agent.waitUntilConnectTimeChanged(agent.getAgentlastSuceededTime());
    //
    // AgentWork agentFalse = new AgentWork("root", "324011", "172.29.17.66", agentCatalog);
    // agentFalse.configureAndStart(true);
    //
    // dataBaseWork.waitToken(agentFalse.getSetId(), 15000, 1000);
    //
    // Client myClient1 = new Client(clientCatalog, false);
    // ClientWork setretai6 = new ClientWork(myClient1, agentFalse.getHost(), agentFalse.getPort());
    // Assert.assertFalse(setretai6.waitIfAgentOnline());
    // setretai6.checkModules(myClient1, false);
    //
    // setretai6.stopCheckerResetStatus();
    // agentFalse.stopAgent();
    //
    // }

    @Description("Получение лицензий, в реестре нет активных лицензий")
    @Severity(SeverityLevel.CRITICAL)
    @Issue("SL-7")
    @Test
    public void test_noActiveLicense() throws Exception {

        AgentWork agent = new AgentWork(agentCatalog);
        agent.configureAndStart(true);

        dataBaseWork.waitToken(agent.getSetId(), 15000, 1000);

        agent.setAndCheckHid(dataBaseWork.getHid(agent.getSetId()), false, false);

        Client myClient = new Client(clientCatalog, false);
        ClientWork setretail10 = new ClientWork(myClient, agent.getHost(), agent.getPort());

        Assert.assertTrue(setretail10.waitIfAgentOnline());
        setretail10.checkModules(myClient, false);
        setretail10.stopCheckerResetStatus();
    }

 /*   @Description("Ошибка при старте агента лицензирования из-за поврежденного локального хранилища")
    @Severity(SeverityLevel.CRITICAL)
    @Issue("SL-7")
    @Test(priority = 1)
    public void test_corruptedData() throws Exception {
        AgentWork agent = new AgentWork(agentCatalog);

        agent.corruptData();
        agent.configureAndStart(false);

        dataBaseWork.waitToken(agent.getSetId(), 15000, 1000);

        agent.setAndCheckHid(dataBaseWork.getHid(agent.getSetId()), false, true);

        Client myClient = new Client(clientCatalog, false);
        ClientWork setretail10 = new ClientWork(myClient, agent.getHost(), agent.getPort());

        Assert.assertFalse(setretail10.waitIfAgentOnline());
        setretail10.checkModules(myClient, false);
        setretail10.stopCheckerResetStatus();
    } */

    @Description("Обновление лицензий,изменения есть: увеличение количества лицензий")
    @Severity(SeverityLevel.CRITICAL)
    @Issue("SL-7")
    @Test
    public void test_UpdateIncreaseLicense() throws Exception {

        AgentWork agent = new AgentWork(agentCatalog);
        agent.configureAndStart(true);

        dataBaseWork.waitToken(agent.getSetId(), 15000, 1000);

        agent.setAndCheckHid(dataBaseWork.getHid(agent.getSetId()), false, false);

        Client myClient = new Client(clientCatalog, true);
        ClientWork setretail10 = new ClientWork(myClient, agent.getHost(), agent.getPort());

        Assert.assertTrue(setretail10.waitIfAgentOnline());
        setretail10.checkModulesNoMainPermission(myClient, false);

        dataBaseWork.updateLicenseCount(1, agent.getSetId());

        agent.waitUntilConnectTimeChanged(agent.getAgentlastSuceededTime());

        setretail10.sendClientInfo();
        Thread.sleep(3000);
        Assert.assertTrue(setretail10.waitIfAgentOnline());
        setretail10.checkModules(myClient, true);

        agent.waitUntilConnectTimeChanged(agent.getAgentlastSuceededTime());

        myClient.saveGuidFromDb(dataBaseWork);
        String s = dataBaseWork.getAgentStat(agent.getSetId());

        AgentStats agentStats = new AgentStats(s);
        agentStats.checkClient(myClient);
        agentStats.checkResult(licenseCount);
        setretail10.stopCheckerResetStatus();
    }

    @Description("Обновление лицензий,изменения есть: уменьшение количества лицензий")
    @Severity(SeverityLevel.CRITICAL)
    @Issue("SL-7")
    @Test
    public void test_UpdateDecreaseLicense() throws Exception {

        AgentWork agent = new AgentWork(agentCatalog);

        agent.configureAndStart(true);

        dataBaseWork.waitToken(agent.getSetId(), 15000, 1000);

        agent.setAndCheckHid(dataBaseWork.getHid(agent.getSetId()), false, false);

        Client myClient = new Client(clientCatalog, true);
        ClientWork setretail10 = new ClientWork(myClient, agent.getHost(), agent.getPort());

        Assert.assertTrue(setretail10.waitIfAgentOnline());
        setretail10.checkModules(myClient, true);

        dataBaseWork.updateLicenseCount(0, agent.getSetId());

        agent.waitUntilConnectTimeChanged(agent.getAgentlastSuceededTime());

        setretail10.sendClientInfo();
        Thread.sleep(1000);
        Assert.assertTrue(setretail10.waitIfAgentOnline());
        setretail10.checkModulesNoMainPermission(myClient, false);

        agent.waitUntilConnectTimeChanged(agent.getAgentlastSuceededTime());

        // Assert.assertTrue(setretail10.waitIfAgentOnline());

        myClient.saveGuidFromDb(dataBaseWork);
        String s = dataBaseWork.getAgentStat(agent.getSetId());

        AgentStats agentStats = new AgentStats(s);

        // TODO change result using file?
        myClient.changeResultModule("alipay1", 0);
        myClient.changeResultModule("setretail10", 0);
        myClient.changeResultModule("module odd", 0);
        agentStats.checkClient(myClient);
        agentStats.checkResult(licenseCount);
        setretail10.stopCheckerResetStatus();
    }

    @Description("Обновление лицензий,изменения есть: изменение количества модулей")
    @Severity(SeverityLevel.CRITICAL)
    @Issue("SL-7")
    @Test
    public void test_UpdateModuleCount() throws Exception {

        AgentWork agent = new AgentWork(agentCatalog);

        agent.configureAndStart(true);

        dataBaseWork.waitToken(agent.getSetId(), 15000, 1000);

        agent.setAndCheckHid(dataBaseWork.getHid(agent.getSetId()), false, false);

        Client myClient = new Client(clientCatalog, true);
        ClientWork setretail10 = new ClientWork(myClient, agent.getHost(), agent.getPort());

        Assert.assertTrue(setretail10.waitIfAgentOnline());
        setretail10.checkModules(myClient, true);

        // TODO :CHECK HERE (?)

        dataBaseWork.updateModulesCount("ALIPAY1", 5);
        dataBaseWork.updateModulesCount("ALIPAY2", 25);
        dataBaseWork.updateModulesCount("module odd", 60);

        agent.waitUntilConnectTimeChanged(agent.getAgentlastSuceededTime());

        // Assert.assertTrue(setretail10.waitIfAgentOnline());

        // setretail10.changeQuantity(myClient, "ALIPAY1", 5);
        // setretail10.changeQuantity(myClient, "ALIPAY2", 20);

        setretail10.sendClientInfo();
        Thread.sleep(3000);
        // TODO change result using file?
        myClient.changeResultModule("alipay1", 5);
        myClient.changeResultModule("alipay2", 20);
        myClient.changeResultModule("module odd",60);
        Assert.assertTrue(setretail10.waitIfAgentOnline());
        setretail10.checkModules(myClient, true);

        agent.waitUntilConnectTimeChanged(agent.getAgentlastSuceededTime());

        // Assert.assertTrue(setretail10.waitIfAgentOnline());

        myClient.saveGuidFromDb(dataBaseWork);
        String s = dataBaseWork.getAgentStat(agent.getSetId());

        AgentStats agentStats = new AgentStats(s);
        agentStats.checkClient(myClient);
        agentStats.checkResult(licenseCount);
        setretail10.stopCheckerResetStatus();
    }

    @Description("Обновление лицензий,изменения есть: добавление модулей")
    @Severity(SeverityLevel.CRITICAL)
    @Issue("SL-7")
    @Test
    public void test_AddModule() throws Exception {

        AgentWork agent = new AgentWork(agentCatalog);
        agent.configureAndStart(true);
        dataBaseWork.waitToken(agent.getSetId(), 15000, 1000);

        agent.setAndCheckHid(dataBaseWork.getHid(agent.getSetId()), false, false);

        Client myClient = new Client(clientCatalog, true);
        ClientWork setretail10 = new ClientWork(myClient, agent.getHost(), agent.getPort());
        // TODO :CHECK HERE (?)
        Assert.assertTrue(setretail10.waitIfAgentOnline());

        dataBaseWork.clearData();

        dataBaseWork.insertProductsWithModules(workCatalog + "test_AddModule/productWithModules_New.json");

        agent.waitUntilConnectTimeChanged(agent.getAgentlastSuceededTime());
        Assert.assertTrue(setretail10.waitIfAgentOnline());

        setretail10.sendClientInfo();
        Assert.assertTrue(setretail10.waitIfAgentOnline());
        Thread.sleep(3000);
        setretail10.checkModules(myClient, true);

        agent.waitUntilConnectTimeChanged(agent.getAgentlastSuceededTime());

        myClient.saveGuidFromDb(dataBaseWork);
        String s = dataBaseWork.getAgentStat(agent.getSetId());

        AgentStats agentStats = new AgentStats(s);
        agentStats.checkClient(myClient);
        agentStats.checkResult(licenseCount);
        setretail10.stopCheckerResetStatus();
    }

    @Description("Обновление лицензий,изменения есть: удаление модулей")
    @Severity(SeverityLevel.CRITICAL)
    @Issue("SL-7")
    @Test
    public void test_DeleteModule() throws Exception {

        AgentWork agent = new AgentWork(agentCatalog);
        agent.configureAndStart(true);
        dataBaseWork.waitToken(agent.getSetId(), 15000, 1000);

        agent.setAndCheckHid(dataBaseWork.getHid(agent.getSetId()), false, false);

        Client myClient = new Client(clientCatalog, true);
        ClientWork setretail10 = new ClientWork(myClient, agent.getHost(), agent.getPort());
        // TODO :CHECK HERE (?)
        Assert.assertTrue(setretail10.waitIfAgentOnline());

        dataBaseWork.deleteModule("ALIPAY2");

        Assert.assertTrue(setretail10.waitIfAgentOnline());
        agent.waitUntilConnectTimeChanged(agent.getAgentlastSuceededTime());

        setretail10.sendClientInfo();
        System.out.println("start client info");
        Thread.sleep(5000);
        System.out.println("end client info");
        setretail10.checkModules(myClient, true);

        agent.waitUntilConnectTimeChanged(agent.getAgentlastSuceededTime());

        Assert.assertTrue(setretail10.waitIfAgentOnline());

        myClient.saveGuidFromDb(dataBaseWork);
        String s = dataBaseWork.getAgentStat(agent.getSetId());

        AgentStats agentStats = new AgentStats(s);
        agentStats.checkClient(myClient);
        agentStats.checkResult(licenseCount);
        setretail10.stopCheckerResetStatus();

    }

    @Description("Обновление лицензий: изменений нет")
    @Severity(SeverityLevel.CRITICAL)
    @Issue("SL-7")
    @Test
    public void test_UpdateLicensesNoChanges() throws Exception {

        AgentWork agent = new AgentWork(agentCatalog);

        agent.configureAndStart(true);

        dataBaseWork.waitToken(agent.getSetId(), 15000, 1000);

        agent.setAndCheckHid(dataBaseWork.getHid(agent.getSetId()), false, false);

        Client myClient = new Client(clientCatalog, true);
        ClientWork setretail10 = new ClientWork(myClient, agent.getHost(), agent.getPort());

        Assert.assertTrue(setretail10.waitIfAgentOnline());
        setretail10.checkModules(myClient, true);

        agent.waitUntilConnectTimeChanged(agent.getAgentlastSuceededTime());

        // Assert.assertTrue(setretail10.waitIfAgentOnline());

        setretail10.sendClientInfo();
        Thread.sleep(1000);
        setretail10.checkModules(myClient, true);

        agent.waitUntilConnectTimeChanged(agent.getAgentlastSuceededTime());
        myClient.saveGuidFromDb(dataBaseWork);
        String s = dataBaseWork.getAgentStat(agent.getSetId());

        AgentStats agentStats = new AgentStats(s);
        agentStats.checkClient(myClient);
        agentStats.checkResult(licenseCount);
        setretail10.stopCheckerResetStatus();
    }

    @Description("Обновление лицензий: добавление продуктов")
    @Severity(SeverityLevel.CRITICAL)
    @Issue("SL-7")
    @Test
    public void test_AddProduct() throws Exception {

        AgentWork agent = new AgentWork(agentCatalog);
        agent.configureAndStart(true);
        dataBaseWork.waitToken(agent.getSetId(), 15000, 1000);

        agent.setAndCheckHid(dataBaseWork.getHid(agent.getSetId()), false, false);

        Client myClient = new Client(clientCatalog, true);
        ClientWork setretail10 = new ClientWork(myClient, agent.getHost(), agent.getPort());

        Assert.assertTrue(setretail10.waitIfAgentOnline());

        Client myClient1 = new Client(workCatalog + "test_AddProduct/clientcatalog2.json", true);
        ClientWork setretail6 = new ClientWork(myClient1, agent.getHost(), agent.getPort());

        Assert.assertFalse(setretail6.waitIfAgentOnline());

        setretail10.checkModules(myClient, true);
        setretail6.checkModules(myClient1, false);

        dataBaseWork.insertProductsWithModules(workCatalog + "test_AddProduct/productWithModules_New.json");

        agent.waitUntilConnectTimeChanged(agent.getAgentlastSuceededTime());

        setretail10.sendClientInfo();
        Thread.sleep(10000);
        setretail10.checkModules(myClient, true);

        setretail6.sendClientInfo();
        Thread.sleep(10000);
        setretail6.checkModules(myClient1, true);

        agent.waitUntilConnectTimeChanged(agent.getAgentlastSuceededTime());

        myClient.saveGuidFromDb(dataBaseWork);
        String s = dataBaseWork.getAgentStat(agent.getSetId());

        AgentStats agentStats = new AgentStats(s);
        agentStats.checkClient(myClient);
        agentStats.checkResult(licenseCount);
        setretail10.stopCheckerResetStatus();
        setretail6.stopCheckerResetStatus();

    }

    @Description("Обновление лицензий:удаление продуктов")
    @Severity(SeverityLevel.CRITICAL)
    @Issue("SL-7")
    @Test
    public void test_DeleteProduct() throws Exception {
        // TODO : FIXME 10 - DAYS
        AgentWork agent = new AgentWork(agentCatalog);
        agent.configureAndStart(true);
        dataBaseWork.waitToken(agent.getSetId(), 15000, 1000);

        agent.setAndCheckHid(dataBaseWork.getHid(agent.getSetId()), false, false);

        Client myClient = new Client(clientCatalog, true);
        ClientWork setretail10 = new ClientWork(myClient, agent.getHost(), agent.getPort());

        Assert.assertTrue(setretail10.waitIfAgentOnline());

        Client myClient1 = new Client(workCatalog + "test_AddProduct/clientcatalog2.json", true);
        ClientWork setretail6 = new ClientWork(myClient1, agent.getHost(), agent.getPort());

        Assert.assertTrue(setretail6.waitIfAgentOnline());

        setretail10.checkModules(myClient, true);
        setretail6.checkModules(myClient1, true);

        dataBaseWork.deleteProduct("SETRETAIL6");
        agent.waitUntilConnectTimeChanged(agent.getAgentlastSuceededTime());

        setretail10.sendClientInfo();
        Thread.sleep(3000);
        setretail10.checkModules(myClient, true);

        setretail6.sendClientInfo();
        Thread.sleep(3000);
        setretail6.checkModules(myClient1, true);

        agent.waitUntilConnectTimeChanged(agent.getAgentlastSuceededTime());

        myClient.saveGuidFromDb(dataBaseWork);
        String s = dataBaseWork.getAgentStat(agent.getSetId());

        AgentStats agentStats = new AgentStats(s);
        agentStats.checkClient(myClient);
        agentStats.checkResult(licenseCount);
        setretail10.stopCheckerResetStatus();
        setretail6.stopCheckerResetStatus();
    }

    @Description("Обновление лицензии модулем, срок действия лицензии продукта подходит  к концу; срок действия лицензии продукта истек")
    @Severity(SeverityLevel.CRITICAL)
    @Issue("SL-7")
    @Test
    public void test_ProductLicenseExpired() throws Exception {
        int diffDays = 5;
        int diffDaysExpired = -1;

        String productname = "SETRETAIL10";

        AgentWork agent = new AgentWork(agentCatalog);

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_WEEK, diffDays);
        Timestamp current = new Timestamp(cal.getTime().getTime());

        dataBaseWork.setProductLicenceEndDate(current, productname, agent.getSetId());

        agent.configureAndStart(true);
        dataBaseWork.waitToken(agent.getSetId(), 15000, 1000);

        agent.setAndCheckHid(dataBaseWork.getHid(agent.getSetId()), false, false);

        Client myClient = new Client(clientCatalog, true);
        ClientWork setretail10 = new ClientWork(myClient, agent.getHost(), agent.getPort());

        Assert.assertTrue(setretail10.waitIfLicensesExpires());
        setretail10.checkLicenseExpiration(cal, myClient.getProduct());
        setretail10.checkModules(myClient, true);

        Calendar cal1 = Calendar.getInstance();
        cal1.add(Calendar.DAY_OF_WEEK, diffDaysExpired);
        Timestamp current1 = new Timestamp(cal1.getTime().getTime());

        dataBaseWork.setProductLicenceEndDate(current1, productname, agent.getSetId());
        agent.waitUntilConnectTimeChanged(agent.getAgentlastSuceededTime());

        setretail10.sendClientInfo();
        Thread.sleep(1000);
        Assert.assertTrue(setretail10.waitIfAgentOnline());
        setretail10.checkModulesNoMainPermission(myClient, false);

        setretail10.stopCheckerResetStatus();
    }

    @Description("Обновление лицензии модулем, срок действия функции подходит  к концу; срок действия функции истек")
    @Severity(SeverityLevel.CRITICAL)
    @Issue("SL-7")
    @Test
    public void test_ModuleLicenseExpired() throws Exception {

        int diffDays = 5;
        int diffDaysExpired = -1;

        String modulname = "ALIPAY1";
        String oddmodule = "module odd";
        String productname = "SETRETAIL10";

        AgentWork agent = new AgentWork(agentCatalog);

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_WEEK, diffDays);
        Timestamp current = new Timestamp(cal.getTime().getTime());

        dataBaseWork.setModuleLicenceEndDate(current, modulname, productname, agent.getSetId());
        dataBaseWork.setModuleLicenceEndDate(current, oddmodule, productname, agent.getSetId());

        agent.configureAndStart(true);
        dataBaseWork.waitToken(agent.getSetId(), 15000, 1000);

        agent.setAndCheckHid(dataBaseWork.getHid(agent.getSetId()), false, false);

        Client myClient = new Client(clientCatalog, true);
        ClientWork setretail10 = new ClientWork(myClient, agent.getHost(), agent.getPort());

        Assert.assertTrue(setretail10.waitIfLicensesExpires());
        setretail10.checkLicenseExpiration(cal, modulname);
        setretail10.checkModules(myClient, true);

        Calendar cal1 = Calendar.getInstance();
        cal1.add(Calendar.DAY_OF_WEEK, diffDaysExpired);
        Timestamp current1 = new Timestamp(cal1.getTime().getTime());

        dataBaseWork.setModuleLicenceEndDate(current1, modulname, productname, agent.getSetId());
        dataBaseWork.setModuleLicenceEndDate(current1, oddmodule, productname, agent.getSetId());
        agent.waitUntilConnectTimeChanged(agent.getAgentlastSuceededTime());

        setretail10.sendClientInfo();

        Thread.sleep(3000);

        Assert.assertTrue(setretail10.waitIfAgentOnline());
        // TODO change result using file?
        myClient.changeResultModule("alipay1", 0);
        myClient.changeResultModule("module odd", 0);
        setretail10.checkModules(myClient, true);
        setretail10.stopCheckerResetStatus();

    }

    @Description("Обновление лицензии модулем, продление срока действия лицензии продукта")
    @Severity(SeverityLevel.CRITICAL)
    @Issue("SL-7")
    @Test
    public void test_ProductLicenseContinueAfterExpiration() throws Exception {
        int diffDays = 5;
        int diffDaysExpired = -1;
        String productname = "SETRETAIL10";

        AgentWork agent = new AgentWork(agentCatalog);

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_WEEK, diffDaysExpired);
        Timestamp current = new Timestamp(cal.getTime().getTime());

        dataBaseWork.setProductLicenceEndDate(current, productname, agent.getSetId());

        agent.configureAndStart(true);
        dataBaseWork.waitToken(agent.getSetId(), 15000, 1000);

        agent.setAndCheckHid(dataBaseWork.getHid(agent.getSetId()), false, false);

        Client myClient = new Client(clientCatalog, true);
        ClientWork setretail10 = new ClientWork(myClient, agent.getHost(), agent.getPort());

        Assert.assertTrue(setretail10.waitIfAgentOnline());
        setretail10.checkModulesNoMainPermission(myClient, false);

        Calendar cal1 = Calendar.getInstance();
        cal1.add(Calendar.DAY_OF_WEEK, diffDays);
        Timestamp current1 = new Timestamp(cal1.getTime().getTime());

        dataBaseWork.setProductLicenceEndDate(current1, productname, agent.getSetId());
        agent.waitUntilConnectTimeChanged(agent.getAgentlastSuceededTime());
        System.out.println("Time Agent Changed");

        setretail10.sendClientInfo();
        Thread.sleep(3000);
        Assert.assertTrue(setretail10.waitIfAgentOnline());

        setretail10.checkModules(myClient, true);
        setretail10.stopCheckerResetStatus();
    }

    @Description("Обновление лицензии модулем, продление срока действия лицензии модуля")
    @Severity(SeverityLevel.CRITICAL)
    @Issue("SL-7")
    @Test
    public void test_ModuleLicenseContinueAfterExpiration() throws Exception {
        int diffDays = 5;
        int diffDaysExpired = -1;
        String productname = "SETRETAIL10";
        String modulname = "ALIPAY1";
        String moduleodd = "module odd";

        AgentWork agent = new AgentWork(agentCatalog);

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_WEEK, diffDaysExpired);
        Timestamp current = new Timestamp(cal.getTime().getTime());

        dataBaseWork.setModuleLicenceEndDate(current, modulname, productname, agent.getSetId());
        dataBaseWork.setModuleLicenceEndDate(current, moduleodd, productname, agent.getSetId());

        agent.configureAndStart(true);
        dataBaseWork.waitToken(agent.getSetId(), 15000, 1000);

        agent.setAndCheckHid(dataBaseWork.getHid(agent.getSetId()), false, false);

        Client myClient = new Client(clientCatalog, true);
        ClientWork setretail10 = new ClientWork(myClient, agent.getHost(), agent.getPort());

        Assert.assertTrue(setretail10.waitIfAgentOnline());
        setretail10.checkModules(myClient, true);

        Calendar cal1 = Calendar.getInstance();
        cal1.add(Calendar.DAY_OF_WEEK, diffDays);
        Timestamp current1 = new Timestamp(cal1.getTime().getTime());

        dataBaseWork.setModuleLicenceEndDate(current1, modulname, productname, agent.getSetId());
        dataBaseWork.setModuleLicenceEndDate(current1, moduleodd, productname, agent.getSetId());
        agent.waitUntilConnectTimeChanged(agent.getAgentlastSuceededTime());

        Assert.assertTrue(setretail10.waitIfAgentOnline());
        setretail10.sendClientInfo();
        Thread.sleep(3000);
        // TODO change result using file
        myClient.changeResultModule("alipay1", 5);
        myClient.changeResultModule("module odd",100);
        setretail10.checkModules(myClient, true);
        setretail10.stopCheckerResetStatus();
    }


}

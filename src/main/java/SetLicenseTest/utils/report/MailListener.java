package SetLicenseTest.utils.report;

import io.qameta.allure.Allure;
import org.testng.*;
import org.testng.reporters.EmailableReporter2;
import org.testng.xml.XmlSuite;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.awt.*;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.google.common.net.HttpHeaders.USER_AGENT;

/**
 * Created by d.fedorov on 10.05.2017.
 */

public class MailListener extends EmailableReporter2 implements ITestListener {

    private final String TC_LOGIN = "d.fedorov@crystals.ru";
    private final String TC_PASSWORD = "32401111";
    private final String TC_IP = "172.20.0.82";

    private static void sendMail(int passed, int failed, int skipped, long time,int buildid) {
        try {
            int all = 0;
            final String username = "autoqa@crystals.ru";
            final String password = "6XCBr&4S";
            final String usernameto = "v5@crystals.ru";
            final String filename = System.getProperty("user.dir") + "/target/surefire-reports/emailable-report.html";
        //    final String content = "<a href=\"http://setv.crystals.ru/repository/download/id_2_id/"+buildid+":id/allure.tar.gz%21/home/setlicense/BuildAgent/work/24d9c9accd0ffd7e/allure-report/index.html\">Press here to open test report</a>";
            all = skipped + passed + failed;

            Properties props = new Properties();
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.port", "587");

            Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {

                    @Override
                    protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                        return new javax.mail.PasswordAuthentication(username, password);
                    }
                });

            // Create a default MimeMessage object.

            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.addRecipient(javax.mail.Message.RecipientType.TO, new InternetAddress(usernameto));
            if (passed != all) {
                message.setSubject("ERROR.Result Autotest.LicensesServer");
            } else {
                message.setSubject("SUCCESS.Result Autotest.LicensesServer");
            }

            BodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setText("All tests have finished. \n" + "Tests: \n" + " All: " + all + "\n" + "  Failed: " + failed + "\n" +
                "  Passed: " + passed + "\n" + "  NotExecuted: " + skipped + "\n" + "Time: " + (time / 100) + " seconds" + "\n");



            //BodyPart messageBodyPartlabel = new MimeBodyPart();
         //   messageBodyPartlabel.setContent(content,"text/html");

            // Create a multipar message
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messageBodyPart);
      //      multipart.addBodyPart(messageBodyPartlabel);

            // Part two is attachment

            System.out.print("Attachment.... "+filename);
            messageBodyPart = new MimeBodyPart();
            DataSource source = new FileDataSource(filename);
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName(filename);
            multipart.addBodyPart(messageBodyPart);
            message.setContent(multipart);
            Transport.send(message);

        } catch (MessagingException mex) {
            mex.printStackTrace();
        }
    }

    @Override
    public void generateReport(List<XmlSuite> xml, List<ISuite> suites, String outdir) {


        super.generateReport(xml, suites, outdir);
        int passed = 0;
        int skipped = 0;
        int failed = 0;
        long time = 0;
        // Prisma SetLicenseTest Suite
        for (ISuite suite : suites) {
            // String suiteName = suite.getName();
            // Getting the results for the said suite
            Map<String, ISuiteResult> suiteResults = suite.getResults();

            for (ISuiteResult sr : suiteResults.values()) {
                // Following code gets the suite name
                // Getting the results for the suite
                passed = sr.getTestContext().getPassedTests().size() + passed;
                failed = sr.getTestContext().getFailedTests().size() + failed;
                skipped = sr.getTestContext().getSkippedTests().size() + skipped;
                time = sr.getTestContext().getEndDate().getTime() - sr.getTestContext().getStartDate().getTime() + time;
            }
        }

        try {
            sendMail(passed, failed, skipped, time, getlastBuildId());
        }

        catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void attachForFailedTests() throws AWTException,InterruptedException,FileNotFoundException {
        final String licenseAgentLog="/opt/SetAgent/logs/LicenseAgent.log";
        final String licenseClientLog=System.getProperty("user.dir")+"/logs/LicenseAgent.log";

        FileInputStream licenseAgent = new FileInputStream(licenseAgentLog);
        FileInputStream licenseClient = new FileInputStream(licenseClientLog);

        Allure.addAttachment("AgentLog",licenseAgent);
        Allure.addAttachment("ClientLog",licenseClient);
    }


    private int getlastBuildId() throws Exception {
        int buildId = 0;

        String url ="http://"+TC_IP+"/app/rest/builds/?locator=lookupLimit:1";

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setReadTimeout(30000);

        // optional default is GET
        con.setRequestMethod("GET");

        // add request header
        con.setRequestProperty("User-Agent", USER_AGENT);

        String basicAuth = Base64.getEncoder().encodeToString((TC_LOGIN + ":" + TC_PASSWORD).getBytes(StandardCharsets.UTF_8));
        con.setRequestProperty("Authorization", "Basic " + basicAuth);


        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //find build_id in xml
        Pattern findid = Pattern.compile("(?<=id:).*?(?=\")");
        Matcher match = findid.matcher(response);

        //choose current build
        while (match.find()) {
            buildId = Integer.valueOf(match.group())+1;
            return  buildId;
        }

        return  buildId;

    }

    @Override
    public void onStart(ITestContext arg0) {

    }

    @Override
    public void onTestStart(ITestResult arg0) {

    }

    @Override
    public void onTestSkipped(ITestResult arg0) {

    }

    @Override
    public void onTestFailure(ITestResult arg0) {
        try {
            attachForFailedTests();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTestSuccess(ITestResult arg0) {

    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult arg0) {

    }

    @Override
    public void onFinish(ITestContext arg0) {
        Reporter.log("Completed executing test " + arg0.getName(), true);
    }

}

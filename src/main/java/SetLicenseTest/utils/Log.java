package SetLicenseTest.utils;


import org.apache.log4j.xml.DOMConfigurator;

/**
 * Created by d.fedorov on 14.04.2017.
 */
public class Log {
    public static org.apache.log4j.Logger logger = null;

    static {
        try {
            DOMConfigurator.configure("SetLog.xml");
            logger = org.apache.log4j.Logger.getLogger("Main");
        } catch (Throwable e) {
            System.out.println("Cannot init logger " + e.getMessage());
            System.exit(1);
        }
    }

}

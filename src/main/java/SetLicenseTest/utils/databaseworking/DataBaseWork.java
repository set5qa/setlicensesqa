package SetLicenseTest.utils.databaseworking;

import SetLicenseTest.utils.Parameters;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;

public class DataBaseWork {

    private String host;
    private String url;
    private String login;
    private String password;
    private Connection con = null;

    static {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private Connection getConnection() throws SQLException {
        if (con == null || con.isClosed()) {
            con = DriverManager.getConnection(url, login, password);
        }
        return con;
    }

    public DataBaseWork() {
        host = Parameters.system.get("dbHost", "127.0.0.1");
        url = "jdbc:postgresql://" + host + ":5432/" + Parameters.system.get("dbName","setl");
        login = Parameters.system.get("dbLogin", "postgres");
        password = Parameters.system.get("dbPassword", "postgres");
    }

    public void clearData() {

        try {
            Connection con = getConnection();
            try (Statement st = con.createStatement()) {
                st.execute("delete from modules;" + "delete from set_products;" + "delete from client_info;" + "delete from set");
            }

        } catch (Exception e) {
            e.printStackTrace();
            con = null;
        }

        // Log.logger.info("Base was cleared");

    }

    public void clearCatalog() {

        try {
            Connection con = getConnection();
            try (Statement st = con.createStatement()) {
                st.execute("delete from modulecatalog;" + "delete from productcatalog;");
            }

        } catch (Exception e) {
            e.printStackTrace();
            con = null;
        }

        // Log.logger.info("Base was cleared");

    }

    public void loadProductCatalog(String data) throws SQLException {
        Connection con = getConnection();
        {
            try (CallableStatement insProduct =
                    con.prepareCall("insert into productcatalog select * from json_populate_recordset(null::productcatalog, ?::json)")) {
                insProduct.setString(1, readDataFromFile(data));
                System.out.println(insProduct);
                insProduct.execute();
            }
        }
    }

    public void loadModuleCatalog(String data) throws SQLException {
        Connection con = getConnection();
        {
            try (CallableStatement insProduct =
                    con.prepareCall("insert into modulecatalog select * from json_populate_recordset(null::modulecatalog, ?::json)")) {
                insProduct.setString(1, readDataFromFile(data));
                System.out.println(insProduct);
                insProduct.execute();
            }
        }
    }

    public void insertProductsWithModules(String data) throws SQLException {
        Connection con = getConnection();
        {
            try (CallableStatement insProduct =
                    con.prepareCall("SELECT load_products(?::json)")) {
                insProduct.setString(1, readDataFromFile(data));
                System.out.println(insProduct);
                insProduct.execute();
            }
        }
    }

    public String getClientguid(String product, Integer shop, Integer deviceNumber, String version) throws SQLException, InterruptedException {
        String guid = "No data";
        int timeout = 15000;
        int t = 0;
        int sleep = 1000;
        Connection con = getConnection();
        {
            try (CallableStatement getGuid = con.prepareCall(
                "select product.client_guid from" +
                    " (select client_guid, value as product from client_info where key ='product') as product" +
                    " left join (select client_guid, value as shop from client_info where key ='shop') as shop" +
                    " on product.client_guid=shop.client_guid" +
                    " left join (select client_guid, value as deviceNumber from client_info where key ='deviceNumber') as deviceNumber" +
                    " on product.client_guid=deviceNumber.client_guid" +
                    " left join (select client_guid, value as version from client_info where key ='version') as version" +
                    " on product.client_guid=version.client_guid" +
                    " where product.product =? and shop.shop =? and deviceNumber.deviceNumber =? and version.version =?")) {
                getGuid.setString(1, product);
                getGuid.setString(2, shop.toString());
                getGuid.setString(3, deviceNumber.toString());
                getGuid.setString(4, version);
                System.out.println(getGuid);
                while (guid == "No data" && t < timeout) {
                    try (ResultSet rs = getGuid.executeQuery()) {
                        if (rs.next()) {
                            guid = rs.getString("client_guid").toString();
                        } else
                            guid = "No data";
                    }
                    Thread.sleep(sleep);
                    t = t + sleep;
                    System.out.println(t);
                }
            }
        }
        System.out.println("Guid from DB = " + guid);
        return guid;
    }

    public String getHid(String setId) throws SQLException {
        String hid = "";
        Connection con = getConnection();
        {
            try (CallableStatement getHid = con.prepareCall("select distinct hid from set where set_id = ?")) {
                getHid.setString(1, setId);
                try (ResultSet rs = getHid.executeQuery()) {
                    if (rs.next()) {
                        hid = rs.getString("hid");
                        if (rs.wasNull()) {
                            hid = null;
                        } else
                            hid = rs.getString("hid").toString();

                    } else
                        hid = "No data";
                }
            }
        }
        return hid;
    }

    public String getToken(String setId) throws SQLException {
        String token = "";
        Connection con = getConnection();
        {
            try (CallableStatement getToken = con.prepareCall("select distinct token_prev from set where set_id = ?")) {
                getToken.setString(1, setId);

                try (ResultSet rs = getToken.executeQuery()) {
                    if (rs.next()) {
                        token = rs.getString("token_prev");
                        if (rs.wasNull()) {
                            token = null;
                        } else
                            token = rs.getString("token_prev").toString();

                    } else
                        token = "No data";
                }
            }
        }
        return token;
    }

    public String waitToken(String setId, int timeout, int sleep) throws SQLException, InterruptedException {
        String token = null;
        int t = 0;
        Connection con = getConnection();
        {
            try (CallableStatement getToken = con.prepareCall("select distinct token_prev from set where set_id = ?")) {
                getToken.setString(1, setId);
                while (token == null && t < timeout) {
                    try (ResultSet rs = getToken.executeQuery()) {
                        if (rs.next()) {
                            token = rs.getString("token_prev");
                            if (rs.wasNull()) {
                                token = null;
                            } else
                                token = rs.getString("token_prev").toString();

                        } else
                            token = "No data";
                    }
                    Thread.sleep(sleep);
                    t = t + sleep;
                    System.out.println(t);
                }

            }
        }
        return token;
    }

    public String getAgentStat(String setId) throws SQLException {
        String agentStat = "";
        Connection con = getConnection();
        {
            try (CallableStatement getStat = con.prepareCall("select value from client_info where set_id =? and key = 'modulesInUse'")) {
                getStat.setString(1, setId);
                try (ResultSet rs = getStat.executeQuery()) {
                    if (rs.next()) {
                        agentStat = rs.getString("value");
                        if (rs.wasNull()) {
                            agentStat = null;
                        } else
                            agentStat = rs.getString("value").toString();

                    } else
                        agentStat = "No data";
                }
            }
        }
        return agentStat;
    }

    public String readDataFromFile(String filename) {
        String content = null;
        try {
            content = new String(Files.readAllBytes(Paths.get(filename)), Charset.forName("UTF-8"));

        } catch (IOException e) {
            e.printStackTrace();
        }
        return content;
    }

    public void updateLicenseCount(int count, String setId) throws SQLException {

        Connection con = getConnection();
        {
            try (CallableStatement insProduct = con.prepareCall("update set_products set license_qnt=? where set_id=?")) {
                insProduct.setInt(1, count);
                insProduct.setString(2, setId);
                System.out.println(insProduct);
                insProduct.execute();
            }
        }
    }

    public void updateModulesCount(String modulename, int count) throws SQLException {
        Connection con = getConnection();
        {
            try (CallableStatement updModuleCount = con.prepareCall(
                "update modules as mod set modulescount = ? from modulecatalog as mc where mc.moduleid = mod.moduleid and mc.modulename= ?")) {
                updModuleCount.setInt(1, count);
                updModuleCount.setString(2, modulename);
                updModuleCount.execute();
            }
        }
    }

    public void deleteModule(String modulname) throws SQLException {

        Connection con = getConnection();
        try (CallableStatement delModule =
                con.prepareCall("delete from modules where moduleid = (select moduleid from modulecatalog where modulename = ?)")) {
            delModule.setString(1, modulname);
            delModule.execute();
        }

    }

    public void deleteProduct(String productname) throws SQLException {

        Connection con = getConnection();

        try (CallableStatement delModule =
                     con.prepareCall("delete from modules where productid = (select productid from productcatalog where productname = ?)")) {
            delModule.setString(1, productname);
            delModule.execute();
        }

        try (CallableStatement delModuleProduct =
                con.prepareCall("delete from set_products where product_id = (select productid from productcatalog where productname = ?)")) {
            delModuleProduct.setString(1, productname);
            delModuleProduct.execute();
        }

    }

    public void setModuleLicenceEndDate(Timestamp date, String modulname, String productname, String setID) throws SQLException {
        Connection connection = getConnection();
        try (CallableStatement changeModuleDate = con.prepareCall(
            "update modules as mod set moduleenddate = ? from  modulecatalog as mc where mc.moduleid = mod.moduleid and mc.modulename = ? and mod.productid= (select product_id from set_products where set_id = ? and product_id = (select productid from productcatalog where productname= ? )) ")) {
            changeModuleDate.setTimestamp(1, date);
            changeModuleDate.setString(2, modulname);
            changeModuleDate.setString(3, setID);
            changeModuleDate.setString(4, productname);
            changeModuleDate.execute();
        }
    }

    public void setProductLicenceEndDate(Timestamp date, String productname, String setID) throws SQLException {
        Connection connection = getConnection();
        try (CallableStatement changeProductDate = con.prepareCall(
            "update set_products as prod set license_end_date = ? from  productcatalog as pc where pc.productid = prod.product_id and pc.productname= ? and prod.set_id = ? ")) {
            changeProductDate.setTimestamp(1, date);
            changeProductDate.setString(2, productname);
            changeProductDate.setString(3, setID);
            changeProductDate.execute();
        }
    }
}

package SetLicenseTest.utils;

import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.xml.sax.Attributes;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Обновляемый список параметров, используемых при обмене и при работе с данными.
 * 
 * @author Владимир Ненахов
 */
public class Parameters {

    public static Parameters system = new Parameters(new File("conf.txt"), "utf-8");

    private Map<String, String> properties = new HashMap<String, String>();
    private Parameters parent = null;

    public Parameters() {
    }

    public Parameters(Element element) {
        updateDataFromElement(element);
    }

    public Parameters(Parameters defaults) {
        this.parent = defaults;
    }

    public Parameters(Attributes attrs) {
        if (attrs != null) {
            for (int i = 0; i < attrs.getLength(); i++) {
                this.put(attrs.getQName(i), attrs.getValue(i));
            }
        }
    }

    public Parameters(File file, String encoding) {
        try {
            List<String> lines = Files.readAllLines(file.toPath(), Charset.forName(encoding));
            lines.forEach(s -> {
                int k = s.indexOf("=");
                if (k > 0) {
                    String name = s.substring(0, k).trim();
                    String value = s.substring(k + 1);
                    put(name, value);
                }
            });
        } catch (Exception e) {
            throw new RuntimeException("Error Loading Properties File '" + file.getName() + "'!  Unable to continue Operation.");
        }
    }

    private static String getKey(String key) {
        if (key == null)
            key = "";
        StringBuilder s = new StringBuilder(key.toLowerCase());
        return s.toString();
    }

    /**
     * Изменяем значение параметра.
     * 
     * @param key
     *            - ключ
     * @param value
     *            - значение
     */
    public void put(String key, String value) {
        if (value == null)
            value = "null";
        properties.put(getKey(key), value);
    }

    public boolean existsKey(String key) {
        return properties.containsKey(key) || (parent != null && parent.existsKey(key));
    }

    /**
     * Меняем параметр, если такой уже есть в списке.
     * 
     * @param key
     * @param value
     */
    public void putIfExists(String key, String value) {
        if (existsKey(key)) {
            put(key, value);
        }
    }

    /**
     * Меняем (добавляем) параметр, только если его еще нет в списке.
     * 
     * @param key
     * @param value
     */
    public void putIfNotExists(String key, String value) {
        if (!existsKey(key)) {
            put(key, value);
        }
    }

    /**
     * Получаем значение параметра.
     * 
     * @param key
     *            - ключ
     * @return - возвращаемое значение
     */

    public String get(String key) {
        String value = properties.get(getKey(key));
        if (value == null && parent != null) {
            value = parent.get(key);
        }
        if ((value != null) && (value.equalsIgnoreCase("null"))) {
            value = null;
        }
        // return Operand.getValue(value); // если есть операнд, то будет замена ? непонятно, что за операнд
        return value;
    }

    /**
     * Получаем значение параметра.
     * 
     * @param key
     *            - ключ
     * @param defaultValue
     *            - значение по умолчанию
     * @return - возвращаемое значение
     */
    public String get(String key, String defaultValue) {
        String value = get(key);
        if (value == null) {
            value = defaultValue;
        }
        value = value.trim();
        return value;
    }

    public String getCore(String key) {
        String value = properties.get(getKey(key));
        if (value == null && parent != null) {
            value = parent.get(key);
        }
        if ((value != null) && (value.equalsIgnoreCase("null"))) {
            value = null;
        }
        return value;
    }

    public Double getD(String key, Double defaultValue) {
        String s = get(key, defaultValue != null ? String.valueOf(defaultValue) : null);
        return s != null ? Double.parseDouble(s.trim()) : null;
    }

    public Integer getI(String key, Integer defaultValue) {
        String s = get(key, defaultValue != null ? String.valueOf(defaultValue) : null);
        return s != null ? Integer.parseInt(s.trim()) : null;
    }

    public Double getD(String key) {
        return getD(key, 0.0);
    }

    public Integer getI(String key) {
        return getI(key, 0);
    }

    public void updateDataFromElement(Element element) {
        if (element != null) {
            NamedNodeMap nnm = element.getAttributes();
            for (int i = 0; i < nnm.getLength(); i++) {
                this.put(nnm.item(i).getNodeName(), nnm.item(i).getNodeValue());
            }
        }
    }

    public Parameters createChildPropFromElement(Element element) {
        Parameters prop = new Parameters(this);
        prop.updateDataFromElement(element);
        return prop;
    }

    public String getStringProperty(String section, String name) {
        return get(section + "." + name);
    }

    public String getStringProperty(String section, String name, String defaultValue) {
        return get(section + "." + name, defaultValue);
    }

    public Integer getIntProperty(String section, String name, Integer defaultValue) {
        return getI(section + "." + name, defaultValue);
    }

    private Map<String, String> getProperties() {
        return this.properties;
    }

    /**
     * из новых параметров, обновляем и/или создаем в уже существующие.
     */
    public void merge(Parameters newParam) {
        Map<String, String> newProperties = newParam.getProperties();
        newProperties.forEach((k, v) -> properties.put(k, v));
    }

}

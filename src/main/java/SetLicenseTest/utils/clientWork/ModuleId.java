package SetLicenseTest.utils.clientWork;

public class ModuleId {

    private String product;
    private String name;

    public static ModuleId getMainId(String product) {
        return new ModuleId(product, product);
    }

    private ModuleId() {
    }

    public ModuleId(String product, String name) {
        this.product = product.toUpperCase();
        this.name = name.toLowerCase();
    }

    public String getProduct() {
        return product;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "ModuleId [product=" + product + ", name=" + name + "]";
    }
}

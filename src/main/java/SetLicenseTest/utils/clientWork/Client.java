package SetLicenseTest.utils.clientWork;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import SetLicenseTest.utils.databaseworking.DataBaseWork;

public class Client {

    private String product = "retail6";
    private int productLicenseCount = 1;
    private int shop = 256;
    private Integer deviceNumber = 30;
    private String version = "1";
    private String guid;
    private String storage;
    private Map<String, Integer> modules = new HashMap<>();
    private Map<String, Integer> resultModules = new HashMap<>();

    public void setResultModules(Map<String, Integer> resultModules) {
        this.resultModules = resultModules;
    }

    public Client(String clientConf, boolean resultFileExists) throws IOException {
        File resultFile = new File(clientConf + "_result.json");
        JSONObject jsonObject = new JSONObject(new String(Files.readAllBytes(Paths.get(clientConf))));

        product = jsonObject.getString("product");
        shop = jsonObject.getInt("shop");
        deviceNumber = jsonObject.getInt("deviceNumber");
        version = jsonObject.getString("version");
        storage = product + "_" + shop + "_" + deviceNumber + "_" + version;
        productLicenseCount = jsonObject.getInt("productLicenseCount");

        if (jsonObject.has("modules")) {

            JSONArray modulesjson = jsonObject.getJSONArray("modules");

            for (int i = 0; i < modulesjson.length(); i++) {
                modules.put(modulesjson.getJSONObject(i).get("module").toString(),
                    Integer.valueOf(modulesjson.getJSONObject(i).get("licensesCount").toString()));
            }
        }

        modules.put(product, productLicenseCount);

        if (resultFileExists) {
            JSONObject jsonObject_result = new JSONObject(new String(Files.readAllBytes(Paths.get(clientConf + "_result.json"))));
            JSONArray modulesjson = jsonObject_result.getJSONArray("modules");

            for (int i = 0; i < modulesjson.length(); i++) {
                System.out.print(Paths.get(clientConf) + "_result.json");
                resultModules.put(modulesjson.getJSONObject(i).get("module").toString().toLowerCase(),
                    Integer.valueOf(modulesjson.getJSONObject(i).get("licensesCount").toString()));
            }
        }

    }

    public void changeResultModule(String moduleName, int value) {
        this.resultModules.put(moduleName, value);
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public void saveGuidFromDb(DataBaseWork dataBaseWork) throws SQLException, InterruptedException {
        this.guid = dataBaseWork.getClientguid(product, shop, deviceNumber, version);
    }

    public String getStorage() {
        return storage;
    }

    public String getProduct() {
        return product;
    }

    public int getShop() {
        return shop;
    }

    public Integer getDeviceNumber() {
        return deviceNumber;
    }

    public Map<String, Integer> getModules() {
        return modules;
    }

    public Map<String, Integer> getResultModules() {
        return resultModules;
    }

    public int getProductLicenseCount() {
        return productLicenseCount;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public void setShop(int shop) {
        this.shop = shop;
    }

    public void setDeviceNumber(Integer deviceNumber) {
        this.deviceNumber = deviceNumber;
    }

    public void setModules(Map<String, Integer> modules) {
        this.modules = modules;
    }

    public void setProductLicenseCount(int productLicenseCount) {
        this.productLicenseCount = productLicenseCount;
    }

    public void addProductToModules() {
        modules.put(product, productLicenseCount);
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

}

package SetLicenseTest.utils.clientWork;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.RollingFileAppender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import ru.crystals.license.client.FileExternalStorage;
import ru.crystals.license.client.LicenseClient;
import ru.crystals.license.client.LicenseClientListener;

public class ClientWork {

    private static final Logger LOG = LoggerFactory.getLogger(ClientWork.class);
    private final static String AUTH = "imei";
    private boolean isAgentOnline = false;
    private boolean isAgentOffline = false;
    private boolean isLicenseExpires = false;
    private boolean onMaxOfflinePeriodExpired = false;
    private LicenseClient licenseClient;
    private String storage = System.getProperty("user.dir") + "/licenses/ext/";

    public ClientWork(Client client, String agentIp, String port) throws Exception {
        Field field = LicenseClient.class.getDeclaredField("instances");
        field.setAccessible(true);
        field.set(null, new ConcurrentHashMap<>());
        licenseClient = LicenseClient.init(client.getProduct(), b -> {
            b.setAgent(agentIp + ":" + port)
                .setTopology(client.getShop(), client.getDeviceNumber())
                .setVersion(client.getVersion())
                .addListener(createListener())
                .setAuth(AUTH)
                .setExternalStorage(new FileExternalStorage(new File(storage + client.getStorage())));

            for (Map.Entry<String, Integer> entry : client.getModules().entrySet()) {

                if (entry.getKey().compareTo(client.getProduct()) != 0) {
                    b.addRequiredModule(entry.getKey(), entry.getValue());
                }

            }
        });

    }

    public void stopCheckerResetStatus() throws Exception {

        isAgentOffline = false;
        isAgentOnline = false;
        isLicenseExpires = false;
        onMaxOfflinePeriodExpired = false;

        Field field = LicenseClient.class.getDeclaredField("licenseChecker");
        field.setAccessible(true);
        Object checker = field.get(licenseClient);
        Method method = checker.getClass().getMethod("stopChecker");
        method.setAccessible(true);
        method.invoke(checker);
    }

    public void sendClientInfo() {
        this.resetStatus();
        this.getLicenseClient().sendClientInfo("version", "1");

    }

    public LicenseClient getLicenseClient() {
        return licenseClient;
    }

    private LicenseClientListener createListener() {
        return new LicenseClientListener() {

            @Override
            public void onLicenseChanged(String moduleId) {
                LOG.info("onLicenseChanged\t{}", moduleId);
                LOG.info("\t\t\t{}", licenseClient.getLicenseInfo(moduleId));
            }

            @Override
            public void onLicenseExpires(String moduleId, int daysLeft) {
                LOG.info("onLicenseExpires\t{}\t{}", moduleId, daysLeft);
                LOG.info("\t\t\t{}", licenseClient.getLicenseInfo(moduleId));
                isLicenseExpires = true;
            }

            @Override
            public void onOfflineModeEnabled() {
                isAgentOffline = true;
                LOG.info("onOfflineModeEnabled");
            }

            @Override
            public void onMaxOfflinePeriodExpired() {
                LOG.info("onMaxOfflinePeriodExpired");
                onMaxOfflinePeriodExpired = true;
            }

            @Override
            public void onAgentOnline() {
                LOG.info("onAgentOnline");
                isAgentOnline = true;
            }
        };
    }

    private boolean getAgentStatus() {
        return isAgentOnline || isAgentOffline;
    }

    private void setAgentStatus(boolean AgentStatus) {
        isAgentOnline = AgentStatus;
        isAgentOffline = AgentStatus;
    }

    public boolean waitIfAgentOnline() throws InterruptedException {

        while (!this.getAgentStatus()) {
            System.out.println("isAgentOnline = " + isAgentOnline);
            System.out.println("isAgentOffline = " + isAgentOffline);
            Thread.sleep(500);
        }
        System.out.println(" after while isAgentOnline = " + isAgentOnline);
        boolean localIsAgentOnline = isAgentOnline;
        this.setAgentStatus(false);
        return localIsAgentOnline;
    }

    public boolean waitIfLicensesExpires() throws InterruptedException {

        while (!isLicenseExpires) {
            Thread.sleep(500);
        }

        boolean localIsLicenseExpired = isLicenseExpires;
        isLicenseExpires = false;
        return localIsLicenseExpired;
    }

    public boolean waitIfOfflinePeriodExpired() throws InterruptedException {

        while (!onMaxOfflinePeriodExpired) {
            Thread.sleep(500);
        }

        boolean localIsOfflinePeriodExpired = onMaxOfflinePeriodExpired;
        onMaxOfflinePeriodExpired = false;
        return localIsOfflinePeriodExpired;
    }

    public void resetStatus() {
        isAgentOffline = false;
        isAgentOnline = false;
        isLicenseExpires = false;
        onMaxOfflinePeriodExpired = false;
    }

    public void checkModules(Client client, boolean haspermission) {

        System.out.println(client.getResultModules());
        System.out.println("Start assert");
        Assert.assertEquals(this.getLicenseClient().hasMainPermission(), haspermission);
        System.out.println("end assert");

        if (haspermission) {
            for (Map.Entry<String, Integer> entry : client.getResultModules().entrySet()) {
                if (entry.getValue().equals(0)) {
                    Assert.assertFalse(this.getLicenseClient().hasPermission(entry.getKey()));
                } else {
                    Assert.assertTrue(this.getLicenseClient().hasPermission(entry.getKey()));
                    Assert.assertEquals(this.getLicenseClient().getLicenseInfo(entry.getKey()).getCount(), (int) entry.getValue());
                }

            }
        }
    }

    public void checkModulesNoMainPermission(Client client, boolean haspermission) {

        System.out.println(client.getResultModules());

        Assert.assertEquals(this.getLicenseClient().hasMainPermission(), haspermission);
        for (Map.Entry<String, Integer> entry : client.getResultModules().entrySet()) {
            Assert.assertFalse(this.getLicenseClient().hasPermission(entry.getKey()));
        }

    }

    public void checkLicenseExpiration(Calendar calendar, String name) {
        long daysBetween = ChronoUnit.DAYS.between(Calendar.getInstance().toInstant(), calendar.toInstant());
        Assert.assertEquals(daysBetween, this.getLicenseClient().getLicenseInfo(name).getDaysLeft());
    }

    public void changeQuantity(Client client, String module, int count) {
        client.getResultModules().put(module, count);
    }

    public static void initLogger() {
        int maxLogDays = 1;
        Level logLevel = Level.ALL;
        LogManager.resetConfiguration();
        ConsoleAppender console = new ConsoleAppender();
        console.setLayout(new PatternLayout(LOGGER_PATTERN));
        console.setThreshold(logLevel);
        console.activateOptions();

        RollingFileAppender fa = new RollingFileAppender();
        fa.setName("FileLogger");
        fa.setFile("logs/LicenseAgent.log");
        fa.setLayout(new PatternLayout(LOGGER_PATTERN));
        fa.setMaxBackupIndex(maxLogDays);
        fa.setThreshold(logLevel);
        fa.setAppend(true);
        fa.activateOptions();
        fa.setMaxBackupIndex(maxLogIndex);
        fa.setMaximumFileSize(1024L * 1024L * logFileSize);

        org.apache.log4j.Logger.getRootLogger().addAppender(console);
        org.apache.log4j.Logger.getRootLogger().addAppender(fa);
    }

    public static void deleteFolder(File folder) throws IOException {
        FileUtils.deleteDirectory(folder);
    }

    private static final String LOGGER_PATTERN = "%d{dd.MM HH:mm:ss.SSS} %-5p [%c{1}]\t%m%n";
    private static int maxLogIndex = 9;
    private static int logFileSize = 5;

}

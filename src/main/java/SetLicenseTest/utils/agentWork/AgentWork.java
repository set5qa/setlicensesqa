package SetLicenseTest.utils.agentWork;

import SetLicenseTest.utils.Parameters;
import com.jcraft.jsch.*;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Vector;

import static com.google.common.net.HttpHeaders.USER_AGENT;

/**
 * Created by d.fedorov on 27.11.2018.
 */
public class AgentWork {

    private final String AGENT_CONFIG_PATH = "/opt/SetAgent/config.conf";
    private final String AGENT_DATA_DB_PATH = "/opt/SetAgent/data/";
    private final String AGENT_DATA_DB_FILE_PATH = "/opt/SetAgent/data/agent.h2.db";
    private final String AGENT_COMMAND_START = "cd /opt/SetAgent && systemctl start setagent.service";
    private final String AGENT_COMMAND_KILL = "cd /opt/SetAgent && systemctl stop setagent.service";
    private final String GET_MODULES = "/api/modules";
    private final String TEST_RESET_TIME = "/api/agent/test/827361827";
    private final int TIMEOUT = 1000;
    private final int TIMEOUT_TEST_METHOD = 90000;
    private final String NO_DATA = "No data";

    private final String AGENT_CORRUPTED_DATA = System.getProperty("user.dir") + "/src/main/java/SetLicenseTest/licenseTest/agent.h2.db";

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(AgentWork.class);

    private String user = "root";
    private String password = "324011";
    private String host = Parameters.system.get("agentHost","172.29.17.76");

    private String port;
    private String setid;
    private String hid;
    private String filepath;

    public AgentWork() {

    }

    public AgentWork(String filepath) {

        this.filepath = filepath;

        Parameters agent = new Parameters(new File(this.filepath), "utf-8");
        this.port = agent.get("port", "8089");
        this.setid = agent.get("setid", "0");
    }

    // use it, if you want to create agents on different VM
    public AgentWork(String user, String password, String host, String filepath) {
        Parameters agent = new Parameters(new File(filepath), "utf-8");

        this.filepath = filepath;
        this.user = user;
        this.password = password;
        this.host = host;
        this.port = agent.get("port", "8089");
        this.setid = agent.get("setid", "0");
    }

    private void changeFile(String filepath, String configpath) throws JSchException, SftpException {

        JSch jsch = new JSch();
        JSch.setConfig("StrictHostKeyChecking", "no");
        Session session = jsch.getSession(user, host);
        session.setPassword(password);
        session.connect();

        ChannelSftp sftpChannel = (ChannelSftp) session.openChannel("sftp");
        sftpChannel.connect();
        LOG.trace("Sucessefully connected to remote agent");

        sftpChannel.put(filepath, configpath);

        System.out.print("Putting" + filepath + " in " + AGENT_CONFIG_PATH);

        session.disconnect();
        sftpChannel.disconnect();

    }

    private void deleteData() throws JSchException, SftpException {

        JSch jsch = new JSch();
        JSch.setConfig("StrictHostKeyChecking", "no");
        Session session = jsch.getSession(user, host);
        session.setPassword(password);
        session.connect();

        ChannelSftp sftpChannel = (ChannelSftp) session.openChannel("sftp");
        sftpChannel.connect();
        LOG.trace("Sucessefully connected to remote agent");

        SftpATTRS attrs;

        try {
            attrs = sftpChannel.stat(AGENT_DATA_DB_PATH);
        } catch (Exception e) {
            LOG.error(AGENT_DATA_DB_PATH + " not found");
            return;
        }

        if (attrs != null) {
            LOG.debug("Directory exists IsDir=" + attrs.isDir());
        } else {
            LOG.debug("Creating dir " + AGENT_DATA_DB_PATH);
            sftpChannel.mkdir(AGENT_DATA_DB_PATH);
        }

        Vector<ChannelSftp.LsEntry> files = sftpChannel.ls(AGENT_DATA_DB_PATH);

        for (ChannelSftp.LsEntry entry : files) {
            if (!entry.getFilename().equals(".") && !entry.getFilename().equals("..")) {
                sftpChannel.rm(AGENT_DATA_DB_PATH + entry.getFilename());
            }
        }

        sftpChannel.rmdir(AGENT_DATA_DB_PATH);
        LOG.debug("Sucessefully deleted agent data directory");

        session.disconnect();
        sftpChannel.disconnect();
    }

    public void startAgent() throws InterruptedException {
        execNixComAndGetRez(user, password, host, AGENT_COMMAND_START);
        Thread.sleep(10000);
        LOG.debug("Agent host " + host + " port " + port + " was started");
    }

    public void corruptData() throws JSchException, SftpException {
        changeFile(AGENT_CORRUPTED_DATA, AGENT_DATA_DB_PATH);
    }

    public void configureAndStart(boolean deleteData) throws JSchException, SftpException, InterruptedException {
        if (deleteData) {
            deleteData();
        }

        changeFile(this.filepath, AGENT_CONFIG_PATH);
        startAgent();
    }

    public void stopAgent() {
        execNixComAndGetRez(user, password, host, AGENT_COMMAND_KILL);
        LOG.debug("Agent host " + host + " port " + port + " was stopped");
    }

    private void execNixComAndGetRez(String user, String password, String host, String command) {

        String rez = "+!";
        int port = 22;

        try {
            JSch jsch = new JSch();
            Session session = jsch.getSession(user, host, port);
            session.setPassword(password);
            session.setConfig("StrictHostKeyChecking", "no");

            // System.out.println("Establishing Connection...");
            session.connect();
            LOG.trace("Sucessefully connected to remote agent");

            // System.out.println("Connection established.");
            Channel channel = session.openChannel("exec");
            ((ChannelExec) channel).setCommand(command); // setting command

            channel.setInputStream(null);

            ((ChannelExec) channel).setErrStream(System.err);

            InputStream in = channel.getInputStream();

            channel.connect();

            byte[] tmp = new byte[1024];

            while (in.available() > 0) {
                int i = in.read(tmp, 0, 1024);
                if (i < 0)
                    break;
                rez = new String(tmp, 0, i);
            }

            if (!channel.isClosed()) {
                try {
                    Thread.sleep(1000);
                } catch (Exception e) {
                    rez = e.toString();
                }
            }

            channel.disconnect();
            session.disconnect();
        } catch (Exception e) {
            rez = e.toString();
        }
        LOG.trace("Return value of command" + rez);
    }

    public String getAgentlastSuceededTime() throws IOException {

        String getres = sendGet(GET_MODULES, false);

        if (getres != null) {
            JSONObject json = new JSONObject(getres);
            getres = json.get("lastSuccessConnectTime").toString();
        }

        LOG.info("Got agentlastSuccesedTime : " + getres);

        return getres;
    }

    public String waitUntilConnectTimeChanged(String time) throws IOException, InterruptedException {
        int alltime = 0;
        sendGet(TEST_RESET_TIME, true);

        String newtime = getAgentlastSuceededTime();

        while (time.equals(newtime) && alltime <= TIMEOUT_TEST_METHOD) {
            Thread.sleep(TIMEOUT);
            newtime = getAgentlastSuceededTime();
            alltime = alltime + TIMEOUT;
        }

        if (alltime > TIMEOUT_TEST_METHOD) {
            LOG.error("Couldn't send info to server in " + TIMEOUT_TEST_METHOD + "ms");
            Assert.fail("Couldn't send info to server");
        }

        LOG.info("Got agentNewSuccesedTime : " + newtime);

        return newtime;
    }

    // HTTP GET request
    private String sendGet(String request, boolean handletimeoutex) throws IOException {

        int read_timeout = 5000;

        String url = "http://" + host + ":" + port + request;

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setReadTimeout(read_timeout);

        // optional default is GET
        con.setRequestMethod("GET");

        // add request header
        con.setRequestProperty("User-Agent", USER_AGENT);

        String basicAuth = Base64.getEncoder().encodeToString(("admin" + ":" + "admin").getBytes(StandardCharsets.UTF_8));
        con.setRequestProperty("Authorization", "Basic " + basicAuth);

        LOG.debug("Sended 'GET' request to URL : " + url);

        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            return response.toString();
        }

        catch (SocketTimeoutException e) {
            if (!handletimeoutex) {
                Assert.fail();
                e.printStackTrace();
            }
            return null;
        }

    }

    public String getPort() {
        return port;
    }

    public String getHost() {
        return host;
    }

    public String getSetId() {
        return setid;
    }

    public String getHid() {
        return hid;
    }

    public String getFilepath() {
        return filepath;
    }

    public void setHid(String hid) {
        this.hid = hid;
        LOG.debug("Agent hid is " + hid);
    }

    public void setAndCheckHid(String hid, boolean noData, boolean isNull) {
        this.hid = hid;
        LOG.debug("Agent hid is " + hid + "here");
        if (isNull) {
            Assert.assertNull(this.getHid());
        } else {
            Assert.assertNotNull(this.getHid());
        }

        if (noData) {
            Assert.assertSame(this.getHid(), NO_DATA);
        } else {
            Assert.assertNotSame(this.getHid(), NO_DATA);
        }
    }

}

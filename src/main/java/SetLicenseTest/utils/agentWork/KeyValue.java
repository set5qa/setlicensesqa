package SetLicenseTest.utils.agentWork;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class KeyValue<K, V> {

    private K key;
    private V value;

    public KeyValue() {
    }

    public KeyValue(K key, V value) {
        this.key = key;
        this.value = value;
    }

    public List<KeyValue<K, V>> convert(Map<K, V> map) {
        List<KeyValue<K, V>> result = new ArrayList<>();
        map.entrySet()
            .forEach(en -> result.add(new KeyValue<>(en.getKey(), en.getValue())));
        return result;
    }

    public K getKey() {
        return key;
    }

    public V getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "KeyValue [key=" + key + ", value=" + value + "]";
    }
}

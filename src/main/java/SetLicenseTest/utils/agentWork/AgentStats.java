package SetLicenseTest.utils.agentWork;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.LoggerFactory;
import org.testng.Assert;

import com.fasterxml.jackson.core.type.TypeReference;
import com.google.gson.Gson;

import SetLicenseTest.utils.clientWork.Client;
import SetLicenseTest.utils.clientWork.ModuleId;

/**
 * Created by m.kolyada
 */
public class AgentStats {

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(AgentStats.class);

    private List<KeyValue<ModuleId, Map<String, Integer>>> agentStat;

    public AgentStats(String stats) throws IOException {
        JsonMapper jm = new JsonMapper(false);

        agentStat = jm.readValue(stats, new TypeReference<List<KeyValue<ModuleId, Map<String, Integer>>>>() {
        });

    }

    public void checkClientOld(Client client) throws IOException {

        for (KeyValue<ModuleId, Map<String, Integer>> stats : this.getAgentStat()) {
            System.out.println(client.getProduct());
            System.out.println(stats.getKey().getProduct());
            if (stats.getKey().getProduct().equals(client.getProduct())) {
                System.out.println(" equal ");
                for (Entry<String, Integer> entry : client.getModules().entrySet()) {
                    if (entry.getKey().toLowerCase().equals(stats.getKey().getName())) {
                        assertEquals(entry.getValue(), stats.getValue().get(client.getGuid()));
                    } else
                        System.out.println(entry.getKey() + " not equal " + stats.getKey().getName());
                }
            } else {
                System.out.println(" not equal");
            }
        }

    }

    public void checkClient(Client client) throws IOException {
        assertFalse(client.getGuid().equals("No data"));
        LOG.debug("Start check fo r client" + client.getGuid());
        for (KeyValue<ModuleId, Map<String, Integer>> stats : this.getAgentStat()) {
            LOG.debug("Product from client = " + client.getProduct());
            LOG.debug("Product from server stats = " + stats.getKey().getProduct());
            if (stats.getKey().getProduct().equals(client.getProduct())) {
                LOG.debug("product " + stats.getKey().getProduct() + " equals " + client.getProduct() + ", continue");
                LOG.debug("search result for module " + stats.getKey().getName());
                if (client.getResultModules().get(stats.getKey().getName()) != null) {
                    LOG.debug("result for module " + stats.getKey().getName() + " found, continue");
                    if (client.getResultModules().get(stats.getKey().getName()) != 0) {
                        LOG.debug("Start assert for module " + stats.getKey().getName());
                        LOG.debug("count from server stats " + stats.getValue().get(client.getGuid()));
                        LOG.debug("count from client result " + client.getResultModules().get(stats.getKey().getName()));
                        assertEquals(stats.getValue().get(client.getGuid()), client.getResultModules().get(stats.getKey().getName()));
                        LOG.debug("finish assert for module " + stats.getKey().getName());
                    } else {
                        LOG.debug("zero license count for module " + stats.getKey().getName());

                    }
                } else {
                    LOG.debug(" no data for  module " + stats.getKey().getName() + " continue");

                }
            } else {
                LOG.debug("product " + stats.getKey().getProduct() + "not equals " + client.getProduct() + " next step");
            }
        }

    }

    public void printStats() throws IOException {

        for (KeyValue<ModuleId, Map<String, Integer>> stats : this.getAgentStat()) {
            System.out.println("for product: " + stats.getKey().getProduct() + " " + stats.getKey().getName());
            Map<String, Integer> value = stats.getValue();
            for (Entry<String, Integer> clients : value.entrySet()) {

                LOG.debug("client " + clients.getKey() + " has " + clients.getValue() + " license");
            }
        }

    }

    public int getModuleCount(String moduleName) throws IOException {
        int count = 0;
        for (KeyValue<ModuleId, Map<String, Integer>> stats : this.getAgentStat()) {
            if (stats.getKey().getName().equals(moduleName)) {
                Map<String, Integer> value = stats.getValue();
                for (Entry<String, Integer> clients : value.entrySet()) {
                    count = count + clients.getValue();
                }
            }

        }
        return count;

    }

    public Map<String, Integer> getResult() throws IOException {
        Map<String, Integer> result = new HashMap<String, Integer>();
        int count = 0;
        for (KeyValue<ModuleId, Map<String, Integer>> stats : this.getAgentStat()) {
            String name = stats.getKey().getName();
            int c = 0;
            Map<String, Integer> value = stats.getValue();
            for (Entry<String, Integer> clients : value.entrySet()) {
                c = c + clients.getValue();
                LOG.debug(c + " for " + name);
            }
            result.put(name, result.getOrDefault(name, 0) + c);
        }
        return result;

    }

    public void checkResult(String expResult) throws IOException {
        Map<String, Integer> result = this.getResult();
        Gson gson = new Gson();
        String json = gson.toJson(result);
        // LOG.debug(json);
        System.out.println(json);
        String expData = new String(Files.readAllBytes(Paths.get(expResult)), Charset.forName("UTF-8"));
        Assert.assertEquals(json.toString(), expData);

    }

    public List<KeyValue<ModuleId, Map<String, Integer>>> getAgentStat() {
        return agentStat;
    }

    public void setAgentStat(List<KeyValue<ModuleId, Map<String, Integer>>> agentStat) {
        this.agentStat = agentStat;
    }

}
